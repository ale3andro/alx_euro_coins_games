/** FROM https://www.kevinleary.net/javascript-get-url-parameters/
 * JavaScript Get URL Parameter
 * 
 * @param String prop The specific URL parameter you want to retreive the value for
 * @return String|Object If prop is provided a string value is returned, otherwise an object of all properties is returned
 */
function getUrlParams( prop ) {
    var params = {};
    var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
    var definitions = search.split( '&' );

    definitions.forEach( function( val, key ) {
        var parts = val.split( '=', 2 );
        params[ parts[ 0 ] ] = parts[ 1 ];
    } );

    return ( prop && prop in params ) ? params[ prop ] : params;
}

$( document ).ready(function() {
    $( "#effect" ).hide();
    var eurosum=0;
    var num_coins=0;
    var max_coins=10;
    var items = [];
    var challenges = 0; // Number of challenges in the activity
    var challenge = 0; // Number of the current activity displayed
    var settings = [];

    if (!('id' in getUrlParams())) {
        $.ajax(
            {   url: "activities/all_activities",
                success: function(result) {
                    $('#alx_clause_title').html('Διαθέσιμες δραστηριότητες');
                    $('#alx_clause_subtitle').html('<ul>');
                    console.log(result);
                    var alldata = result.split('\n');
                    for (var i=0; i<alldata.length; i+=3) {
                        if (alldata[i]!='') {
                            var ttitle = alldata[i+1].substring(1, alldata[i+1].length-1);
                            var newLinePosition = ttitle.search('<br />');
                            if (newLinePosition!=-1)
                                ttitle=ttitle.substring(0, newLinePosition);
                            ttitle = "Τάξη " + alldata[i+2].substring(1, alldata[i+2].length-1) + " - " + ttitle;
                            $('#alx_clause_subtitle').html($('#alx_clause_subtitle').html() + "<li><a href='?id=" + alldata[i].substring(0, alldata[i].length-5) + "'>" + ttitle + "</a></li>");
                        }
                    }
                    $('#alx_clause_subtitle').html($('#alx_clause_subtitle').html() + '</ul>');
                    $('#alx_clauses').html('');
                },
                error: function(result) {
                    console.log('failure');
                } 
            });
        return; 
    }
      
    var jqxhr = $.getJSON("activities/" + getUrlParams('id') + ".json", function(data) {
            title = data['metadata']['title'];
            subtitle = data['metadata']['subtitle'];
            settings = data['metadata'];
            $.each(data['items'], function(index, value) {
                items.push(value);
            });
            $('#alx_header').text(settings['title'] + " | " + settings['subtitle']);
            challenges = items.length;
            
            $('#money_cost').text(items[0].value);
            $('#item_image').attr("src", data['metadata']['img_folder'] + "/" + items[0].img);
            
            if (data['metadata']['show_one_euro_coin']=='false')
                $('#alx_euro1').hide();
            if (data['metadata']['show_two_euros_coin']=='false')
                $('#alx_euro2').hide();
    })
    .fail(function() {
        $('#alx_clause_title').html('Μη διαθέσιμη δραστηριότητα');
        $('#alx_clause_subtitle').html('Δεν βρέθηκε δραστηριότητα με αυτό το id');
        $('#alx_clauses').html('');
    });

    $( "#cent01" ).click(function() {
        $("#item_0_1").append("<img src='img/coins/cent_01.png'>");
        update_money_given(1);
    });
    $( "#cent02" ).click(function() {
        $("#item_0_1").append("<img src='img/coins/cent_02.png'>");
        update_money_given(2);
    });
    $( "#cent05" ).click(function() {
        $("#item_0_1").append("<img src='img/coins/cent_05.png'>");
        update_money_given(5);
    });
    $( "#cent10" ).click(function() {
        $("#item_0_1").append("<img src='img/coins/cent_10.png'>");
        update_money_given(10);
    });
    $( "#cent20" ).click(function() {
        $("#item_0_1").append("<img src='img/coins/cent_20.png'>");
        update_money_given(20);
    });
    $( "#cent50" ).click(function() {
        $("#item_0_1").append("<img src='img/coins/cent_50.png'>");
        update_money_given(50);
    });
    $( "#euro1" ).click(function() {
        $("#item_0_1").append("<img src='img/coins/euro_01.png'>");
        update_money_given(100);
    });
    $( "#euro2" ).click(function() {
        $("#item_0_1").append("<img src='img/coins/euro_02.png'>");
        update_money_given(200);
    });
    $("#button_reset_score").click(function() {
        reset_money_given();
    });
    $("#button_check").click(function() {
        clear_imgs_in_div();
        if (eurosum==items[challenge].value) {
            $("#emoji_face").attr('src', 'img/emoji_happy.png');
            $("#effect" ).show( 'shake', {}, 1000, callback_success );
        } else { 
            $("#emoji_face").attr('src', 'img/emoji_sad.png');
            $("#effect" ).show( 'shake', {}, 500, callback_failure );
        }
    });

    function callback_failure() {
        setTimeout(function() {
          $( "#effect:visible" ).removeAttr( "style" ).fadeOut();
          reset_money_given();
        }, 1000 );
        
    };

    function callback_success() {
        setTimeout(function() {
          $( "#effect:visible" ).removeAttr( "style" ).fadeOut();
          reset_money_given();
        }, 1000 );
        next_challenge();
    };

    function next_challenge() {
        challenge++;
        if (challenge<challenges) {
            $('#alx_gift_num').text(challenge+1);
            percent = 100 * challenge / items.length;
            $('#alx_progress').css('width', percent + '%');
            $('#money_cost').text(items[challenge].value);
            $('#item_image').attr("src", settings['img_folder'] + "/" + items[challenge].img);
            reset_money_given();
        }
        else {
            $('#alx_progress').css('width','100%');
            $("#item_image").attr('src', 'img/happy_winner.gif');
        }
    }

    function clear_imgs_in_div() {
        $('#item_0_1 img').each(function() {
            if ($(this).attr('id')!='emoji_face')
                $(this).remove();
        });
    }


    function reset_money_given() {
        eurosum=0;
        num_coins=-1;
        $("#item_0_1").html('');
        $("#item_0_1").html('<div class="toggler"><div id="effect" class="ui-widget-content ui-corner-all"><p><img id="emoji_face" src="img/emoji_happy.png"></p></div></div>');
        $( "#effect" ).hide();
        update_money_given(0);
    }

    function update_money_given(value) {
        num_coins++;
        eurosum+=value;
        if (eurosum>=100) {
            $("#money_given").html(Math.floor(eurosum/100) + " ευρώ και " + eurosum%100);
        } else 
            $("#money_given").html(eurosum);
        if (num_coins==max_coins) {
            alert('Έφθασες τον μέγιστο αριθμό κερμάτων που μπορείς να δώσεις!');
            reset_money_given();
        }
    }


});